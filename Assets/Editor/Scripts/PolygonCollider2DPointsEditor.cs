﻿using UnityEngine;
using UnityEditor;
using System.Collections;

//[CustomEditor(typeof(PolygonCollider2D))]
public class PolygonCollider2DPointsEditor : Editor {
    bool expanded = false;

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        var current = target as PolygonCollider2D;
        var points = current.points;

        expanded = EditorGUILayout.Foldout(expanded, "Edit Points");
        if (expanded) {
            EditorGUI.indentLevel++;

            bool oldChanged = GUI.changed;
            GUI.changed = false;

            for (int i = 0; i < points.Length; i++) {
                points[i] = EditorGUILayout.Vector2Field("Point " + i, points[i]);
            }

            if (GUI.changed)
                current.points = points;

            GUI.changed = oldChanged;

            EditorGUI.indentLevel--;
        }
    }
}
