using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

//[CanEditMultipleObjects]
[CustomEditor(typeof(CameraBounds))]
public class CameraBoundsEditor : Editor {
	CameraBounds current;
	
	/*override public void OnInspectorGUI() {
		current = (PolygonCollider) target;
		
		current.depth = EditorGUILayout.FloatField("Depth", current.depth);
		EditorGUILayout.EnumPopup("Normal Generation", PolygonCollider.NormalGenerationMode.None);
		current.caps = (PolygonCollider.ColliderCap) EditorGUILayout.EnumPopup("Collider Caps", current.caps);
		current.flatMesh = EditorGUILayout.Toggle("Flatten Render Mesh", current.flatMesh);
	}*/
	
	public void OnSceneGUI() {
		current = (CameraBounds) target;
		
		Transform t = current.transform;
		
		Handles.color = new Color(1,1,1,1.0f);
		
		Vector3[] worldPts = DrawRectPoly(current.bounds, t);
		Handles.DrawPolyLine(worldPts);
	}

	private static Vector3[] DrawRectPoly(Rect r, Transform t) {
		Vector3[] worldPts = new Vector3[5];

		worldPts[0] = new Vector3(r.xMin, r.yMin);
		worldPts[1] = new Vector3(r.xMax, r.yMin);
		worldPts[2] = new Vector3(r.xMax, r.yMax);
		worldPts[3] = new Vector3(r.xMin, r.yMax);
		worldPts[4] = worldPts[0];

		for (int i = 0; i < worldPts.Length; i++) {
			worldPts[i] = t.TransformPoint(worldPts[i]); 
		}

		return worldPts;
	}
}
