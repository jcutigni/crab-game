using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

[CanEditMultipleObjects]
[CustomEditor(typeof(Polygon2DRenderer))]
public class Polygon2DRendererEditor : Editor {
    override public void OnInspectorGUI() {
        EditorGUI.BeginChangeCheck();
        DrawDefaultInspector();
        bool changed = EditorGUI.EndChangeCheck();

        //Debug.LogFormat("Event.current.type {0}, commandName {1}", Event.current.type, Event.current.commandName);
        if ((Event.current.type == EventType.ValidateCommand || Event.current.type == EventType.Used) && Event.current.commandName == "UndoRedoPerformed")
            changed = true;

        if (changed) {
            foreach (var t in targets) {
                if (t is Polygon2DRenderer) {
                    UpdateMesh((Polygon2DRenderer)t, true);
                }
            }
        }
    }

    void OnSceneGUI() {
        if (target is Polygon2DRenderer) {
            UpdateMesh((Polygon2DRenderer)target);
        }
    }

    private static void UpdateMesh(Polygon2DRenderer renderer, bool force = false) {
        var collider = renderer.GetComponent<PolygonCollider2D>();
        if (collider) {
            Vector2[] points = collider.points;
            if (IsDifferent(points, renderer.points)) {
                //renderer.points = points;
                renderer.frontTriangles = Triangulator.Triangulate(points);
                force = true;
            }
        }

        if (force) {
            renderer.BuildMesh();
            EditorUtility.SetDirty(renderer);
        }
    }

    private static bool IsDifferent(Vector2[] a, Vector2[] b) {
        if (a == b) {
            return false;
        }

        if (a == null || b == null || a.Length != b.Length) {
            return true;
        }

        for (int i = 0; i < a.Length; i++) {
            if (a[i] != b[i]) {
                return true;
            }
        }
        return false;
    }

    [MenuItem("GameObject/Create Other/Polygon Collider With Renderer", false, 12900)]
    static void DoCreateRenderingColliderObject() {
        GameObject go = new GameObject("Polygon Collider");

        Polygon2DRenderer collider = go.AddComponent<Polygon2DRenderer>();
        go.AddComponent<MeshFilter>();
        MeshRenderer meshRenderer = go.AddComponent<MeshRenderer>();

        go.AddComponent<PolygonCollider2D>();

        UpdateMesh(collider);
        collider.caps = Polygon2DRenderer.MeshType.FrontAndBack;
        collider.BuildMesh();

        GameObject primitive = GameObject.CreatePrimitive(PrimitiveType.Plane);
        primitive.SetActive(false);
        Material diffuse = primitive.GetComponent<MeshRenderer>().sharedMaterial;
        DestroyImmediate(primitive);

        meshRenderer.material = diffuse;

        Selection.activeGameObject = go;
        Undo.RegisterCreatedObjectUndo(go, "Create Polygon Collider");
    }
}
