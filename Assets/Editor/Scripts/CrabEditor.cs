﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Crab))]
public class CrabEditor : Editor {
    void OnSceneGUI() {
        var current = (Crab)target;

        var transform = current.transform;
        foreach (var faked in current.fakeContactCasts) {
            var origin = transform.TransformPoint(faked.point);
            var direction = transform.TransformDirection(faked.direction).normalized;

            Handles.color = Color.yellow;
            Handles.DrawLine(origin - direction * faked.pullback, origin + direction.normalized * Physics2D.minPenetrationForPenalty * 3);
        }
    } 
}
