﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	[SerializeField]
    Transform _target;
	public Transform target {
		get { return _target; }
		set {
			_target = value ? value.transform : null;
			body = value ? value.GetComponent<Rigidbody2D>() : null;

            Vector3 p = _target ? _target.position : transform.position;
            prevP = p;
            currP = p;
        }
	}
	[SerializeField] public string defaultTargetName;
	
	Rigidbody2D body;
	
	Vector3 prevP;
	Vector3 currP;
	float lastUpdate = 0f;

	//float tanVFOV;
	//float tanHFOV;
	//float lastAspect;

	new private Camera camera;

	[SerializeField] public float vOffset = 0f;

    [SerializeField]
	CameraBounds bounds;

	// Use this for initialization
	void Start () {
		camera = GetComponent<Camera>();

		TargetSearch();

        target = _target;

        //const float baseAspect = 16f / 9f;
        //tanVFOV = Mathf.Tan(Mathf.Deg2Rad * camera.fieldOfView * 0.5f);
        //// camera correct horizontally intially at 16:9
        //tanHFOV = tanVFOV * baseAspect;

        ////camera.fieldOfView = Mathf.Atan(tanHFOV / camera.aspect) * Mathf.Rad2Deg * 2f;
        //lastAspect = camera.aspect;
    }

	void OnLeveWasLoaded(int level) {
		TargetSearch();
	}

	void TargetSearch() {
		if (target == null) {
			GameObject go = GameObject.Find(defaultTargetName);
            if (go)
			    target = go.transform;
		}
	}
	
	// Update is called once per frame
	void LateUpdate () {
		float t = (Time.time - lastUpdate) / Time.fixedDeltaTime;
		Vector3 p = Vector3.Lerp(prevP, currP, t);
		p.z = transform.position.z;
		transform.position = p;

        // fix the fov around the screen width:
        // vFOV = atan(tan(hFOV) / aspect)

        //if (lastAspect != camera.aspect) {
        //    lastAspect = camera.aspect;
        //    camera.fieldOfView = Mathf.Atan(tanHFOV / camera.aspect) * Mathf.Rad2Deg * 2f;
        //}
    }

	void FixedUpdate() {
		if (target) {
			lastUpdate = Time.time;
			prevP = currP;
			Vector3 velocity = body ? (Vector3) body.velocity : (currP - prevP) / Time.fixedDeltaTime;
			currP += ((_target.position - velocity * 0.025f) - currP) * 0.2f;

			if (bounds) {
				Vector3 camPos = bounds.transform.InverseTransformPoint(currP);
				float dz = -bounds.transform.InverseTransformPoint(transform.position).z;

				float halfHeight = Mathf.Tan(Mathf.Deg2Rad * camera.fieldOfView * 0.5f) * dz;
				float halfWidth = halfHeight * camera.aspect;

				Rect boundsRect = bounds.bounds;
				boundsRect = new Rect(boundsRect.x + halfWidth, boundsRect.y + halfHeight, boundsRect.width - halfWidth * 2f, boundsRect.height - halfHeight * 2f);

				// Horizontal bounds
				bool edge = false;
				if (camPos.x < boundsRect.xMin) {
					camPos.x = boundsRect.xMin;
					edge = true;
				}
				if (camPos.x > boundsRect.xMax) {
					camPos.x = edge ? boundsRect.center.x : boundsRect.xMax;
				}
				// Vertical bounds
				edge = false;
				if (camPos.y < boundsRect.yMin) {
					camPos.y = boundsRect.yMin;
					edge = true;
				}
				if (camPos.y > boundsRect.yMax) {
					camPos.y = edge ? boundsRect.center.y : boundsRect.yMax;
				}

				camPos.y += vOffset;

				currP = bounds.transform.transform.TransformPoint(camPos);
			}
		}
	}
}
