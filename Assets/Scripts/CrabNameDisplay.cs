﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CrabNameDisplay : MonoBehaviour {
    public Crab crab;

    [SerializeField]
    Canvas canvas;
    [SerializeField]
    RectTransform tagTransform;
    [SerializeField]
    Text nameText;

	void LateUpdate() {
	    if (crab) {
            var camera = Camera.main;
            if (camera) {
                var screenPoint = camera.WorldToScreenPoint(crab.transform.position);
                Vector3 worldPos;
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle((RectTransform)canvas.transform, screenPoint, canvas.worldCamera, out worldPos)) {
                    tagTransform.position = worldPos;
                }

                nameText.text = crab.playerName;
            }
        } else {
            Destroy(gameObject);
        }
	}
}
