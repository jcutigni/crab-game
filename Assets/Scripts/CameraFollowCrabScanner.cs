﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[RequireComponent(typeof(CameraFollow))]
public class CameraFollowCrabScanner : MonoBehaviour {
    CameraFollow follow;

    void Awake() {
        follow = GetComponent<CameraFollow>();
    }

	void Update() {
        if (follow && !follow.target) {
            foreach (var crab in Object.FindObjectsOfType<Crab>()) {
                if (crab.isLocalPlayer) {
                    follow.target = crab.transform;
                }
            }
        }
	}
}
