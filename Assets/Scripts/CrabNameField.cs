﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(InputField))]
public class CrabNameField : MonoBehaviour {
    public const string NameKey = "playerName";

    void OnEnable() {
        var tf = GetComponent<InputField>();
        if (tf) {
            tf.text = PlayerPrefs.GetString(NameKey);
            tf.onValueChange.AddListener(OnValueChange);
        }
    }

    void OnDisable() {
        var tf = GetComponent<InputField>();
        if (tf) {
            tf.onValueChange.RemoveListener(OnValueChange);
        }
    }

    public void OnValueChange(string value) {
        PlayerPrefs.SetString(NameKey, value);
    }
}
