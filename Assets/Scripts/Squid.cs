﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

#pragma warning disable 219, 414

public class Squid : MonoBehaviour {
    [SerializeField] Rigidbody2D body;

    public int castCount;
    public float castRadius;
    public float castBaseWidth;
    public Vector2 castOrigin;

    // The ideal is the target displacement is where we're always at the max somewhere
    // and with slopes the back side is sunk up to the minimum
    public float maxLength = 0.9f;
    public float minLength = 0.3f;
    public float steepestAngle = 60f;

    public float maxJumpHeight = 1.5f;
    public float minJumpHeight = 0.5f;
    public float jetpackTime = 1f;

    public float landSpeed = 1280.0f / 2.0f / 220.0f;
    public float friction = 1f;


    float maxT = 2f;
    float currT = 0.5f;
    float maxDY = 0.5f;

    Vector2[] points;
    RaycastHit2D[] hits;

	void OnEnable() {
        hits = new RaycastHit2D[castCount];
        points = new Vector2[castCount];
	}
	
	void Update() {
	
	}

    void FixedUpdate() {
		float hx = Input.GetAxis("Horizontal");

        // acceleration: massless force
        Vector2 a = Vector2.zero;

        var origin = (Vector2)transform.TransformPoint(castOrigin);
        var side = Vector2.right;

        float castDistance = maxLength + maxLength - minLength - castRadius;

        float spacing = castBaseWidth / (castCount - 1);
        float offset = castBaseWidth / 2f;
        for (int i = 0; i < castCount; i++) {
            float x = spacing * i - offset;
            var point = origin + (side * x);

            points[i] = point;
            hits[i] = Physics2D.CircleCast(point, castRadius, Vector2.down, castDistance * 1.5f);
        }

        int hitCount = hits.Count(hit => hit);
        if (hitCount > 0) {
            float yMax = hits.Where(hit => hit).Min(hit => hit.point.y) + maxLength;  // lowest point + max length
            float yMin = hits.Where(hit => hit).Max(hit => hit.point.y) + minLength;  // highest point + minimum length
            float yTarget = Mathf.Max(yMax, yMin);

            //float targetDeltaY = origin.y - yTarget;
            float targetDeltaY = yTarget - origin.y;

            //Debug.DrawRay(new Vector2(body.position.x, yTarget) + Vector2.left * 0.3f, Vector3.right * 0.6f);
            Debug.DrawLine(new Vector2(body.position.x, yTarget), new Vector2(body.position.x, origin.y));

            // ammount of time we should target achieving that height
            //float dt = 0.01f;
            float dt = 0.00f;
            // the change in y after t seconds if we don't do anything
            float currentDeltaY = body.velocity.y * dt + 0.5f * Physics2D.gravity.y * dt * dt;

            if (targetDeltaY > 0f) {
                //// TODO: find derevation of this, add to notes
                float targetVelocityY = Mathf.Sqrt(-2f * Physics2D.gravity.y * targetDeltaY);
                float deltaVelocityY = targetVelocityY - body.velocity.y;
                float legForce = deltaVelocityY / Time.fixedDeltaTime;
                a.y += legForce;

                //float dyCausedTimeReset = Mathf.Abs(targetDeltaY) / maxDY * maxT;
                //currT = Mathf.Clamp(currT - dyCausedTimeReset, 0f, maxT);

                //float targetVelocityY = -2f * currT * targetDeltaY * Mathf.Exp(-(currT * currT));

                //float deltaVelocityY = targetVelocityY - body.velocity.y;
                //float deltaVYThisFrame = deltaVelocityY / Time.fixedDeltaTime;
                //a.y += deltaVYThisFrame;
                ////a.y -= Physics2D.gravity.y;

                //currT += dt;

                //body.position = new Vector2(body.position.x, yTarget + 2.95f / 2f);
                //body.velocity = new Vector2(body.velocity.x, Mathf.Max(body.velocity.y, 0f));
                //body.AddForce(Physics2D.gravity * -1 * body.mass);
            }
        }


        int groundContactCount = hits.Count(hit => hit && hit.distance <= maxLength);
        // Horizontal movement
        if (groundContactCount > 0)
        {
            const float maxForce = 100f;

            Vector2 slopeLeft = Slopeness(hits, 0, 1);
            Vector2 slopeRight = Slopeness(hits, castCount - 1, -1);
            float angleLeft = Vector2.Angle(slopeLeft, Vector2.left);
            float angleRight = Vector2.Angle(slopeRight, Vector2.right);
            //Debug.LogFormat("{0} {1} : {2} {3}", slopeLeft, slopeRight, angleLeft, angleRight);

            bool steepLeft = Steepness(hits, 0, 1) > steepestAngle;
            bool steepRight = -Steepness(hits, castCount - 1, -1) > steepestAngle;

            if (steepLeft)
                hx = Mathf.Max(0f, hx);
            if (steepRight)
                hx = Mathf.Min(0f, hx);

            Vector2 tangent = Vector2.right;
            if (Mathf.Abs(hx) > 0f)
                tangent = hx > 0f ? slopeRight.normalized : slopeLeft.normalized;

			Vector2 targetSpeed = tangent * Mathf.Abs(hx) * landSpeed;
            Vector2 acceleration = (targetSpeed - body.velocity) / Time.fixedDeltaTime;
            float len = acceleration.magnitude;
            len = Mathf.Clamp(len, -maxForce, maxForce);
            acceleration = acceleration.normalized * len;

            a += acceleration;
        }

        a.y = Math.Max(0f, a.y);
        body.AddForce(a * body.mass);
    }

    Vector2 Slopeness(RaycastHit2D[] hits, int offset, int m = 1) {
        for (int i = 0; i < castCount; i++) {
            var hit = hits[i * m + offset];
            if (hit) {
                for (int j = i + 1; j < castCount; j++) {
                    var other = hits[j * m + offset];
                    if (other) {
                        float dy = hit.point.y - other.point.y;
                        float dx = hit.point.x - other.point.x;
                        return new Vector2(dx, dy);
                    }
                }
            }
        }
        return new Vector2(-m, 0f);
    }

    float AngleFromVector(Vector2 v) {
        return -Mathf.Atan(v.y / v.x) * Mathf.Rad2Deg;
    }

    float Steepness(RaycastHit2D[] hits, int offset, int m = 1) {
        for (int i = 0; i < castCount; i++) {
            var hit = hits[i * m + offset];
            if (hit) {
                for (int j = i + 1; j < castCount; j++) {
                    var other = hits[j * m + offset];
                    if (other) {
                        float dy = other.point.y - hit.point.y;
                        float dx = other.point.x - hit.point.x;
                        float angle = -Mathf.Atan(dy / dx) * Mathf.Rad2Deg;
                        return angle;
                    }
                }
            }
        }
        return 0f;
    }

    void OnDrawGizmos() {
        if (Application.isPlaying) {
            //var origin = (Vector2)transform.TransformPoint(castOrigin);
            Func<RaycastHit2D, bool> filter = hit => hit && hit.distance <= maxLength;
            foreach (var hit in hits) {
                if (hit) {
                    Gizmos.color = Color.Lerp(Color.red, Color.green, filter(hit) ? 1f : 0f);
                    //Debug.DrawLine(origin, hit.centroid);
                    Gizmos.DrawWireSphere(hit.centroid, castRadius);
                }
            }
        }
    }
}
