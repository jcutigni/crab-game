﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;

public class Crab : NetworkBehaviour {
    [SerializeField]
    Rigidbody2D body;
    [SerializeField]
    Vector2 centerOfMass = new Vector2(0f, -0.2f);

    [Header("Movement Properties")]
    [SerializeField]
    [Tooltip("The impulse that gets applied when you first press a direction")]
    float sideBumpImpulse = 30f;
    [SerializeField]
    float maxVelocity = 3f;
    [SerializeField]
    float maxAcceleration = 0.2f;

    [Header("Colision Bug Workaround")]
    [SerializeField]
    public FakedRay[] fakeContactCasts;

    List<ContactPoint> contacts = new List<ContactPoint>();

    float hx = 0;
    bool bumpSideNextUpdate;

    [SyncEvent]
    public event UpdateControlsDelegate EventControls;
    public delegate void UpdateControlsDelegate(float hx);

    public string playerName;
    CrabNameDisplay nameTag;

	void Start() {
        body.centerOfMass = centerOfMass;

        EventControls += (h) => hx = h;

        // movement pattern to reporoduce contact bug
        //hx = -1;
        //yield return new WaitForSeconds(2.2f);
        //hx = 0;
        //yield return new WaitForSeconds(1f);
        //hx = -1; // and now we're stuck
        //yield break;

        var prefab = Resources.Load<CrabNameDisplay>("CrabName");
        nameTag = Instantiate(prefab);
        nameTag.crab = this;
	}

    public override void OnStartClient() {
        base.OnStartClient();

        var client = NetworkClient.allClients[0];
        client.RegisterHandler(CrabMsgType.BodyUpdate, OnBodyUpdate);
    }

    public override void OnStartLocalPlayer() {
        base.OnStartLocalPlayer();

        if (isLocalPlayer) {
            playerName = PlayerPrefs.GetString(CrabNameField.NameKey);

            if (isClient) {
                CmdSetName(playerName);
            }
        }
    }

    void OnBodyUpdate(NetworkMessage message) {
        if (!isServer) {
            var bodyUpdate = message.ReadMessage<BodyUpdateMessage>();

            if (bodyUpdate.id == netId) {
                bodyUpdate.Apply(body);
            }
        }
    }

    // happens after FixedUpdate, so next FixedUpdate will use these contacts
    void OnCollisionStay2D(Collision2D collision) {
        foreach (var contact in collision.contacts) {
            contacts.Add(new ContactPoint(contact));
        }
    }

    void FixedUpdate() {
        AddFakePoints();

        if (body.velocity.magnitude > 0f && body.angularVelocity > 0f) {
            SetDirtyBit(1);
        }

        // Manual control
        if (isLocalPlayer) {
            var h = Input.GetAxisRaw("Horizontal");
            if (h != hx) {
                SetDirtyBit(1);
                CmdUpdateHX(h);
            }
        }

        var transform = this.transform;
        var contacts = this.contacts.Where(c => {
            var p = transform.InverseTransformPoint(c.point);
            return p.y <= 0.1892184f && Mathf.Abs(p.x) > 0.1f;
        }).ToList();

        bool serverSendUpdateNow = false;

        if (Mathf.Abs(hx) > 0f) {
            if (contacts.Count > 0) {
                // find the best point to bump up at if we started moving
                Vector2 forwardPoint = contacts[0].point;

                // spread the force out over all the contact points
                float multiplier = 1f / contacts.Count;
                foreach (var c in contacts) {
                    var localPoint = transform.InverseTransformPoint(c.point);
                    var rotation = Quaternion.Euler(0f, 0f, body.angularVelocity * Time.fixedDeltaTime);
                    var rotatedPoint = rotation * localPoint;
                    var pointVelocity = (Vector2)(rotatedPoint - localPoint) / Time.fixedDeltaTime + body.velocity;

                    if ((hx > 0f && c.point.x > forwardPoint.x) || (hx < 0f && c.point.x < forwardPoint.x)) {
                        forwardPoint = c.point;
                    }

                    Vector2 normal = c.normal;
                    float sign = Mathf.Sign(hx * normal.y); // * tangent.x to pick tangent side facing x direction we want to go
                    Vector2 tangent = new Vector2(normal.y, -normal.x) * sign;

                    if (tangent.y < -0.9f)
                        tangent.y *= -1f;

                    float tangentSpeed = Vector3.Dot(pointVelocity, tangent);
                    float dv = Mathf.Clamp(maxVelocity - tangentSpeed, 0f, maxAcceleration);
                    float magnitude = dv / Time.fixedDeltaTime;

                    //Vector2 force = tangent * 10f * multiplier * body.mass;
                    Vector2 force = tangent * magnitude * multiplier * body.mass;

                    if (tangent.y > 0.5f) {
                        force = force.normalized * (force.magnitude + Physics2D.gravity.magnitude * body.mass * 0.5f);
                    }

                    body.AddForceAtPosition(force, c.point);
                    Debug.DrawRay(c.point, force * 0.1f, Color.red);
                }

                if (bumpSideNextUpdate) {
                    serverSendUpdateNow = true;
                    body.AddForceAtPosition(transform.TransformVector(Vector2.up) * sideBumpImpulse * body.mass, forwardPoint, ForceMode2D.Impulse);
                }
            }
        }

        // reset the bump up
        bumpSideNextUpdate = false;

        if (serverSendUpdateNow) {
            ServerSendUpdateNow();
        }

        // Clear for the following round of OnCollisionStay2Ds
        this.contacts.Clear();
    } 

    [Command]
    void CmdSetName(string name) {
        playerName = name;
        RpcSetName(name);
    }

    [ClientRpc]
    void RpcSetName(string name) {
        playerName = name;
    }

    [Command]
    void CmdUpdateHX(float h) {
        if (hx == 0f && h != 0f)
            bumpSideNextUpdate = true;

        hx = h;
        EventControls(h);
    }

    void ServerSendUpdateNow() {
        if (isServer) {
            NetworkServer.SendUnreliableToReady(this.gameObject, CrabMsgType.BodyUpdate, new BodyUpdateMessage(netId, body));
        }
    }

    public override bool OnSerialize(NetworkWriter writer, bool initialState) {
        writer.Write(body.position);
        writer.Write(body.velocity);
        writer.Write(body.rotation);
        writer.Write(body.angularVelocity);
        writer.Write(hx);

        if (initialState) {
            writer.Write(playerName);
        }

        return true;
    }

    public override void OnDeserialize(NetworkReader reader, bool initialState) {
        body.position = reader.ReadVector2();
        body.velocity = reader.ReadVector2();
        body.rotation = reader.ReadSingle();
        body.angularVelocity = reader.ReadSingle();
        hx = reader.ReadSingle();

        if (initialState) {
            playerName = reader.ReadString();
        }
    }

    // Workaround for unity bug where some contact points are missing:
    // Use raycasts to fake the contact points at important corners
    void AddFakePoints() {
        Physics2D.queriesStartInColliders = false;

        float tolerance = Physics2D.minPenetrationForPenalty * 3;
        foreach (var fake in fakeContactCasts) {
            var origin = transform.TransformPoint(fake.point);

            foreach (var contact in contacts) {
                if (Vector2.Distance(origin, contact.point) <= tolerance)
                    goto next;
            }

            var direction = transform.TransformDirection(fake.direction.normalized);
            float distance = fake.pullback + tolerance;

            var hit = Physics2D.Raycast(origin - direction * fake.pullback, direction, distance);
            if (hit) {
                contacts.Add(new ContactPoint(hit));
            }

            //Debug.DrawLine(origin - direction * fake.pullback, origin + direction * tolerance);

            next: { }
        }
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.green;
        foreach (var contact in contacts) {
            Gizmos.DrawSphere(contact.point, 0.1f);
        }
    }

    class BodyUpdateMessage : MessageBase {
        public NetworkInstanceId id;
        public Vector2 position;
        public Vector2 velocity;
        public float rotation;
        public float angularVelocity;

        public BodyUpdateMessage() { }

        public BodyUpdateMessage(NetworkInstanceId id, Rigidbody2D body) {
            this.id = id;
            position = body.position;
            velocity = body.velocity;
            rotation = body.rotation;
            angularVelocity = body.angularVelocity;
        }

        public void Apply(Rigidbody2D body) {
            body.position = position;
            body.velocity = velocity;
            body.rotation = rotation;
            body.angularVelocity = angularVelocity;
        }
    }

    private struct ContactPoint {
        public Collider2D collider;
        public Vector2 normal;
        public Vector2 point;
        public ContactPoint(ContactPoint2D real) {
            collider = real.collider;
            normal = real.normal;
            point = real.point;
        }
        public ContactPoint(RaycastHit2D hit) {
            collider = hit.collider;
            normal = hit.normal;
            point = hit.point;
        }
    }

    [System.Serializable]
    public struct FakedRay {
        public Vector2 point;
        public Vector2 direction;
        public float pullback;
    }
}
