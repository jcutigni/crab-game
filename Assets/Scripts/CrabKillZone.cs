﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CrabKillZone : MonoBehaviour {
    void OnTriggerEnter2D(Collider2D other) {
        var go = other.gameObject;
        var crab = go.GetComponent<Crab>();
        if (crab && crab.isServer) {
            var newCrab = Instantiate(NetworkManager.singleton.playerPrefab);
            NetworkServer.Spawn(newCrab);
            NetworkServer.ReplacePlayerForConnection(crab.connectionToClient, newCrab, crab.playerControllerId);

            NetworkServer.Destroy(go);
        }
    }
}
