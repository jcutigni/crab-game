using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine.Serialization;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(PolygonCollider2D))]
[ExecuteInEditMode]
public class Polygon2DRenderer : MonoBehaviour {
    public enum MeshType {
        Flat = 0,
        Front = 1,
        Back = 2,
        FrontAndBack = 3,
    }

    //[HideInInspector]
    //[SerializeField]
    //[FormerlySerializedAs("verticies")]
    [NonSerialized]
    public Vector2[] points; // just a cache
    [HideInInspector]
    [SerializeField]
    [FormerlySerializedAs("capIndicesFront")]
    public int[] frontTriangles;

    [SerializeField]
    [FormerlySerializedAs("_depth")]
    private float m_depth;
    [SerializeField]
    [FormerlySerializedAs("_caps")]
    private MeshType m_meshType;

    [SerializeField]
    public float uvScale = 0.2f;

    public float depth
    {
        get { return m_depth; }
        set
        {
            if (value != m_depth) {
                m_depth = value;
                BuildMesh();
            }
        }
    }

    public MeshType caps
    {
        get { return m_meshType; }
        set
        {
            if (value != m_meshType) {
                m_meshType = value;
                BuildMesh();
            }
        }
    }

    Mesh mesh;

    void Awake() {
#if UNITY_EDITOR
        MeshFilter filter = GetComponent<MeshFilter>();
        if (filter) {
            filter.mesh = null;
        }
#endif

        BuildMesh();
    }

    void OnDestroy() {
        if (mesh) {
#if UNITY_EDITOR
            DestroyImmediate(mesh);
#else
			Destroy(mesh);
#endif
        }
    }

    public void BuildMesh() {
        EnsureMeshIfNeeded();
        if (mesh == null) {
            return;
        }

        var collider = GetComponent<PolygonCollider2D>();
        if (collider) {
            points = collider.points;
        }

        MeshFilter filter = GetComponent<MeshFilter>();

        if (filter != null && points != null && points.Length >= 3 && frontTriangles != null) {
            int pointCount = points.Length;

            Vector3[] verticies;
            Vector2[] uvs;
            int[] triangles;

            if (m_meshType == MeshType.Flat) {
                verticies = new Vector3[pointCount];
                uvs = new Vector2[pointCount];

                for (int i = 0; i < pointCount; i++) {
                    var point = points[i];
                    verticies[i] = point;
                    uvs[i] = point * uvScale;
                }

                triangles = frontTriangles;
            } else {
                int capCount = m_meshType == MeshType.FrontAndBack ? 2 : 1;

                int vertCountForStrips = pointCount * 2;
                int vertCountForCaps = pointCount * capCount;

                int vertCount = vertCountForStrips + vertCountForCaps;
                verticies = new Vector3[vertCount];
                uvs = new Vector2[vertCount];

                // side strip verticies
                for (int i = 0; i < pointCount; i++) {
                    Vector2 point = points[i];
                    int vertsOffset = i * 2;

                    verticies[vertsOffset] = new Vector3(point.x, point.y, -m_depth);
                    verticies[vertsOffset + 1] = new Vector3(point.x, point.y, m_depth);

                    uvs[vertsOffset] = point * uvScale;
                    uvs[vertsOffset + 1] = point * uvScale;
                }

                int triLengthForStrip = pointCount * 6;
                int triLengthForCaps = frontTriangles.Length * capCount;
                triangles = new int[triLengthForStrip + triLengthForCaps];

                // side triangles
                int[] stripTriOffsets = { 0, 1, 3, 0, 3, 2 };
                for (int i = 0; i < pointCount; i++) {
                    int triOffset = i * 6;
                    int vertOffset = i * 2;

                    for (int j = 0; j < 6; j++) {
                        int vertIndex = vertOffset + stripTriOffsets[j];
                        if (vertIndex > vertCountForStrips)
                            vertIndex -= vertCountForStrips;

                        triangles[triOffset + j] = vertIndex;
                    }
                }

                // front and back caps
                int startVertIndex = vertCountForStrips;
                int startTriIndex = triLengthForStrip;

                if (m_meshType == MeshType.Front || m_meshType == MeshType.FrontAndBack) {
                    for (int i = 0; i < pointCount; i++) {
                        var point = points[i];
                        int vertIndex = i + startVertIndex;
                        verticies[vertIndex] = new Vector3(point.x, point.y, -m_depth);
                        uvs[vertIndex] = point * uvScale;
                    }

                    for (int i = 0; i < frontTriangles.Length; i++) {
                        triangles[startTriIndex + i] = frontTriangles[i] + startVertIndex;
                    }

                    startVertIndex += points.Length;
                    startTriIndex += frontTriangles.Length;
                }

                if (m_meshType == MeshType.Back || m_meshType == MeshType.FrontAndBack) {
                    for (int i = 0; i < pointCount; i++) {
                        var point = points[i];
                        int vertIndex = i + startVertIndex;
                        verticies[vertIndex] = new Vector3(point.x, point.y, m_depth);
                        uvs[vertIndex] = point * uvScale;
                    }

                    for (int i = 0; i < frontTriangles.Length; i += 3) {
                        // flip triangles
                        triangles[startTriIndex + i + 0] = frontTriangles[i + 2] * 2 + 1;
                        triangles[startTriIndex + i + 1] = frontTriangles[i + 1] * 2 + 1;
                        triangles[startTriIndex + i + 2] = frontTriangles[i + 0] * 2 + 1;
                    }

                    startVertIndex += points.Length;
                    startTriIndex += frontTriangles.Length;
                }
            }

            int n = verticies.Length;
            Color[] colors = new Color[n];
            for (int i = 0; i < n; i++) {
                colors[i] = Color.white;
            }

            mesh.Clear();
            mesh.vertices = verticies;
            mesh.uv = uvs;
            mesh.colors = colors;
            mesh.triangles = triangles;

            mesh.RecalculateNormals();

            filter.sharedMesh = null;
            filter.sharedMesh = mesh;

#if !UNITY_EDITOR
			mesh.Optimize();
#endif
        }
    }

    void EnsureMeshIfNeeded() {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        if (meshFilter != null) {
            mesh = meshFilter ? meshFilter.sharedMesh : null;

            if (mesh == null) {
                mesh = new Mesh();
                meshFilter.sharedMesh = mesh;
            }

#if UNITY_EDITOR
            mesh.MarkDynamic();
#endif
            mesh.hideFlags = HideFlags.DontSave;
        }
    }

#if UNITY_EDITOR
    void OnDrawGizmos() {
        MeshRenderer r = GetComponent<MeshRenderer>();
        if (r == null || !r.enabled) {
            Gizmos.DrawIcon(transform.position, "Polygon Collider", true);
        }
    }
#endif
}
